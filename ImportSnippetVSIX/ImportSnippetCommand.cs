﻿//------------------------------------------------------------------------------
// <copyright file="ImportSnippetCommand.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Media;
using EnvDTE80;
using ImportSnippetVSIX.Properties;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace ImportSnippetVSIX
{

	/// <summary>
	/// Command handler
	/// </summary>
	internal sealed class ImportSnippetCommand
	{

		/// <summary>
		/// Command ID.
		/// </summary>
		public const int CommandId = 0x0100;

		/// <summary>
		/// Command menu group (command set GUID).
		/// </summary>
		public static readonly Guid CommandSet = new Guid("71dbb497-3660-4a4b-ac47-f79b0f4df873");

		/// <summary>
		/// VS Package that provides this command, not null.
		/// </summary>
		private readonly Package package;

		/// <summary>
		///
		/// </summary>
		private DTE2 dte2;

		/// <summary>
		/// Initializes the singleton instance of the command.
		/// </summary>
		/// <param name="package">Owner package, not null.</param>
		public static void Initialize(AsyncPackage package)
		{
			Instance = new ImportSnippetCommand(package);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportSnippetCommand"/> class.
		/// Adds our command handlers for menu (commands must exist in the command table file)
		/// </summary>
		/// <param name="package">Owner package, not null.</param>
		private ImportSnippetCommand(AsyncPackage package)
		{
			this.package = package ?? throw new ArgumentNullException(nameof(package));
			package.JoinableTaskFactory.SwitchToMainThreadAsync();
			this.dte2 = this.ServiceProvider.GetService(typeof(SDTE)) as DTE2;
			package.JoinableTaskFactory.SwitchToMainThreadAsync();
			if (this.ServiceProvider.GetService(typeof(IMenuCommandService)) is OleMenuCommandService commandService)
			{
				var menuCommandID = new CommandID(CommandSet, CommandId);
				var menuItem = new OleMenuCommand(this.MenuItem_Invoke, this.MenuItem_CommandChanged, this.MenuItem_BeforeQueryStatus, menuCommandID);
				commandService.AddCommand(menuItem);
			}
		}

		private void MenuItem_CommandChanged(object sender, EventArgs e)
		{
		}

		private List<EnvDTE.ProjectItem> _selectedSnippetItems = new List<EnvDTE.ProjectItem>();

		/// <summary>
		///
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MenuItem_BeforeQueryStatus(object sender, EventArgs e)
		{
			var command = sender as OleMenuCommand;
			var selectedItems = this.dte2.ToolWindows.SolutionExplorer.SelectedItems as object[];
			this._selectedSnippetItems.Clear();
			foreach (EnvDTE.UIHierarchyItem selectedUIHierarchyItem in selectedItems)
			{
				if (selectedUIHierarchyItem.Object is EnvDTE.ProjectItem)
				{
					EnvDTE.ProjectItem item = selectedUIHierarchyItem.Object as EnvDTE.ProjectItem;

					if (item.Name.EndsWith(".snippet"))
					{
						this._selectedSnippetItems.Add(item);
					}
				}
			}
			command.Visible = this._selectedSnippetItems.Count > 0;

			command.Text = this._selectedSnippetItems.Count == 1 ? Resources.CommandTextSelectedOne : Resources.CommandTextSelectedMany;
		}

		/// <summary>
		/// Gets the instance of the command.
		/// </summary>
		public static ImportSnippetCommand Instance
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the service provider from the owner package.
		/// </summary>
		private IServiceProvider ServiceProvider => this.package;

		private SnippetsManager _snippetsManager;

		/// <summary>
		/// Gets the service provider from the owner package.
		/// </summary>
		private SnippetsManager SnippetsManager => this._snippetsManager ?? (this._snippetsManager = new SnippetsManager(this.ServiceProvider));

		/// <summary>
		/// This function is the callback used to execute the command when the menu item is clicked.
		/// See the constructor to see how the menu item is associated with this function using
		/// OleMenuCommandService service and MenuCommand class.
		/// </summary>
		/// <param name="sender">Event sender.</param>
		/// <param name="e">Event args.</param>
		private void MenuItem_Invoke(object sender, EventArgs e)
		{
			var failed = new List<string>();
			foreach (var item in this._selectedSnippetItems)
			{
				if (!this.SnippetsManager.Import(item))
				{
					failed.Add(item.Name);
				}
			}
			if (failed.Count > 0)
			{
				SystemSounds.Hand.Play();
				VsShellUtilities.ShowMessageBox(
				 this.ServiceProvider,
				 string.Join(Environment.NewLine, failed),
				 Resources.ImportFialedHeader.PadRight(50),
				 OLEMSGICON.OLEMSGICON_WARNING,
				 OLEMSGBUTTON.OLEMSGBUTTON_OK,
				 OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
			}
			else
			{
				SystemSounds.Beep.Play();
			}
		}
	}
}
