﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using EnvDTE;
using Microsoft.VisualStudio.Settings;
using Microsoft.VisualStudio.Shell.Settings;

namespace ImportSnippetVSIX
{

	internal class SnippetsManager
	{

		public SnippetsManager(IServiceProvider serviceProvider)
		{
			this._serviceProvider = serviceProvider;
		}

		private string _codeSnippetsPath = null;

		private string CodeSnippetsPath
		{
			get
			{
				if (string.IsNullOrEmpty(this._codeSnippetsPath))
				{
					SettingsManager settingsManager = new ShellSettingsManager(this._serviceProvider);
					return this._codeSnippetsPath = Path.Combine(settingsManager.GetApplicationDataFolder(ApplicationDataFolder.Documents), "Code Snippets");
				}
				else
				{
					return this._codeSnippetsPath;
				}
			}
		}

		private static readonly Dictionary<string, string> LanguageDirectory = new Dictionary<string, string> {
			{ "JavaScript", @"JavaScript\My Code Snippets" },
			{ "SQL_SSDT", @"SQL_SSDT\My Code Snippets" },
			{ "VB", @"Visual Basic\My Code Snippets" },
			{ "csharp", @"Visual C#\My Code Snippets" },
			{ "cpp", @"Visual C++\My Code Snippets" },
			{ "css", @"Visual Web Developer\My CSS Snippets" },
			{ "html", @"Visual Web Developer\My HTML Snippets" },
			{ "XAML", @"XAML\My XAML Snippets" },
			{ "XML", @"XML\My Xml Snippets" },
			//{ "", @"" },
		};

		/// <summary>
		///
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private string GetLanguageOfProjectItem(EnvDTE.ProjectItem item)
		{
			if (item.FileCount == 1)
			{
				var doc = XDocument.Load(item.FileNames[1]);
				var lang = doc.Root.Descendants().Where(e => e.Name.LocalName == "Code").Attributes("Language").FirstOrDefault();
				return lang?.Value;
			}

			return null;
		}

		private IServiceProvider _serviceProvider;

		internal bool Import(ProjectItem item)
		{
			string lang = this.GetLanguageOfProjectItem(item);
			if (lang != null && LanguageDirectory.ContainsKey(lang))
			{
				var targetDirectory = Path.Combine(this.CodeSnippetsPath, LanguageDirectory[this.GetLanguageOfProjectItem(item)]);
				try
				{
					if (!Directory.Exists(targetDirectory))
					{
						Directory.CreateDirectory(targetDirectory);
					}

					var targetFile = Path.Combine(targetDirectory, item.Name);
					File.Copy(item.FileNames[1], targetFile, true);
					File.SetAttributes(targetFile, FileAttributes.Archive); //ignore source attributes
				}
				catch
				{
					return false;
				}
				return true;
			}

			return false;
		}
	}
}
